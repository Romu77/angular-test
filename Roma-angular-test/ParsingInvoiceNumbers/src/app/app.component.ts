import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'ParsingInvoiceNumbers';
  text:any;
  ngOnInit(){
    this.parse();
  }
  parse(){   
     document.getElementById('files').addEventListener('change', this.handleFileSelect, false);
  }
  handleFileSelect=(evt)=> {
    var files = evt.target.files; // FileList object
    console.log('files',evt.target.files)
      var reader = new FileReader();
      var self = this; 
      console.log('self',self);
      
        reader.onload = (response) =>{
        self.text=reader.result;
        self.print(self.text);
    };
    reader.readAsText(files[0]);
  }
 get7segment(ascii) {
  var array=ascii.split('\n');
  console.log('array',array)
  var outputarr=[];
    while(array.length){
      var output=this.showOutput(array);
      console.log('output',output);
     
      outputarr.push(output);
      console.log('outputarr',outputarr);
    }
  return outputarr;
}
 showOutput(array){
  var arr=array.splice(0,4);
  return  arr.reduce(function (r, a, i) {
    console.log(a,'a');
    a.match(/.../g).forEach(function (b, j) {
        r[j] = r[j] || [];
        r[j][i] = b;
    });
    return r;
  }, []).
  map(function (a) {            
      return a.join('');
  }).
  map(function (a) {
      console.log('a',a);
      var bits = { 63: 0, 6: 1, 91: 2, 79: 3, 102: 4, 109: 5, 125: 6, 7: 7, 127: 8, 111: 9, 0: ' ' },
          v = '909561432'.split('').reduce(function (r, v:any, i) {
            var d:any=(a[i] !== ' ');
              return r + (d << v);
          }, 0);
      return v in bits ? bits[v] : '*'; 
  }).
  join('');
}
 print(ascii) {
    var pre = document.createElement('pre');
    pre.innerHTML = ascii + '\n\n' + this.get7segment(ascii);
    document.body.appendChild(pre);
}

}
