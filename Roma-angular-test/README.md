Ques 1. Marco Polo Game

For Solution go to MarcoPolo Game Folder and then run index.html file the output will be shown to the browser and also to the console.

Ques 2. Angular 2+ Questions

Ques 2.1 Imports

Import is basically used to import functionality used in another modules which can be built in as well as modules madeby us such as components,pipes,services etc. It is  basically what we add in imports in the @NgModule Decorator.
By importing that module we can use everything whatever it exports. 

Declarations

Declarations basically includes components, directives  and pipes .

Providers-
Providers are usually singleton (one instance) objects, that other objects have access to through dependency injection (DI).

Ques 2.2
Components-
Components are the most basic UI building block of an Angular app. It consists o template and classes.Template includes html which is used for rendering and class. It contains properties and methods. This has the code which is used to support the view. It is defined in TypeScript.

Directives-
A directive is a custom HTML element that is used to extend the power of HTML. 
Two types of directives are-
Structural directives—It change the DOM layout by adding and removing DOM elements.
Ex-*ngIf,*ngFor
Attribute directives—It change the appearance or behavior of an element, component, or another directive.
Ex-ngClass,ngStyle etc;

Models-
Models are objects which hold our data.
Ex- Creating a user model

export class User {
  id: number;
  name: string;
}

Modules-
Modules  refers to a place where you can group the components, directives, pipes, and services, which are related to the application. To define module, we can use the NgModule.It starts with @NgModule and contains an object which has declarations, imports, providers and bootstrap.

Services-
Services are used for data communication among componenets especially among siblings.Services are injected in providers array.By including in providers array in a module its instance is created at the root.

Ques -3 For Parsing Invoice numbers go to Parsing Invoice numbers folder and then run ng serve.

